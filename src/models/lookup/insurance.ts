import { Knex } from 'knex';
export class InsuranceService {
  /**
   * บันทึกข้อมูล Admit
   * @param db 
   * @param data 
   * @returns 
   */
  list(db: Knex) {
    let sql = db('insurance')
    return sql.orderBy('name')
  }

  listTotal(db: Knex) {
    let sql = db('insurance')
    return sql.count({ total: '*' });
  }
}